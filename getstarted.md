# Saiku Analytics with Apache Drill

## Adding Data To Apache Drill

### Acccessing the UI

To access the Apache Drill user interface run the following

```
juju expose apache-drill
```

Then navigate to the machine IP address on port 8047 on your browser.

### Testing Drill

Apache Drill is supplied with a sample dataset that allows you to test its functionality quickly and easily.

To use this dataset you can run a query similar to the following within the Apache Drill UI:

```
SELECT * FROM cp.`employee.json` LIMIT 20
```

or

```
SELECT full_name, salary, gender FROM cp.`employee.json` where gender = 'F'
```

As you can see from the query, this is infact running SQL directly over a JSON file. Pretty neat huh?

### Importing data

Now is a good time to start importing your data. For this guide we will supply some test data available [here](https://www.dropbox.com/s/wx9pzx7n4dpxp89/sales.tar.bz2?dl=1).

To upload it to the server you can run:

```
juju scp sales.tar.bz2 apache-drill/0:~
```

This has uploaded the test CSV file to the server.

Next we unzip it and place it within the data storage pool. 

```
juju ssh apache-drill/0
sudo mv sales.tar.bz2 /var/snap/apache-drill-spicule/common
cd /var/snap/apache-drill-spicule/common
sudo bunzip2 sales.tar.bz2
sudo tar xvf sales.tar
```
It is now available for interrogation.

### Querying data

You'll have noticed in the previous operation we unzipped multiple CSV files. One great feature of Drill is directory level querying of identically formatted files. Lets take a look.

Firstly in Drill click on the storage tab and Update the DFS plugin.

Below the tmp block add the following
```
,"test": {
      "location": "/var/snap/apache-drill-spicule/common",
      "writable": true,
      "defaultInputFormat": null,
      "allowAccessOutsideWorkspace": false
    }
```
Within the CSV block set the following:

```
"skipFirstLine": false,
"extractHeader": true,
```


Then click the update button.

Lets start with a query like:

```
select * from dfs.test.`/sales` limit 10
```

This should return the first 10 rows of sales data.
The files are chunked by year so lets try another query:

```
select distinct `Year` from dfs.test.`/sales`
```

As you can see, we're now running SQL over multiple CSV files in a single query as it should return 2012, 2013, 2014. This allows you to chunk up your data into smaller CSV blocks and still interrogate the data as one.

### Creating Views

To use file based data sources within Saiku we need to create a View over the files to allow Saiku to discover the structure. To do this we can run the following:

```
create view dfs.test.`sales_view` as select `Retailer_country`, `Order_method_type`, `Retailer_type`, `Product_line`, `Product_type`, `Product`, `Year`, CAST(`Revenue` as float) as `Revenue`, CAST(`Quantity` AS int) as `Quantity` from dfs.test.`/sales` limit 10;
```
This takes most of our sales set and sqaushes it into 1 view ready for future consumption.


## Getting started with Saiku Analytics

Firstly we'll expose Saiku Analytics

```
juju expose saiku-enterprise
```
Then navigate to the machine IP address on port 8080 on your browser.

Next login with admin/admin.

When you click on the A icon on the toolbar, you should see that there is an apache-drill datasource already populated. This is due to the relation created using Juju.

Click on it, then click Create Schema.

When the popup appears select DRILL from databases available and dfs.test from Database Schema then hit save.

In the new schema designer, select sales_view from the fact table and click add.

Then select the auto populate button from the top of the left column.

Finally hit save.

Back in the main Saiku UI click the New icon, top left then press the green refresh icon. Your new schema should then be visible in the drop down menu in the designer view, where you can now start analysing the test data.

## Enabling Support

To start recieving support for any of the software within this bundle simply activate your SLA for your chosen software, make sure you have credit in your wallet, and support will be activated.

## Advanced 

### Creating Parquet Files for super quick data access

```
create table dfs.test.`/salesparquet/` as select `Retailer_country`, `Order_method_type`, `Retailer_type`, `Product_line`, `Product_type`, `Product`, `Year`, CAST(`Revenue` as float) as `Revenue`, CAST(`Quantity` AS int) as `Quantity` from dfs.test.`/sales`;
```

```
create view dfs.test.`sales_view2` as select `Retailer_country`, `Order_method_type`, `Retailer_type`, `Product_line`, `Product_type`, `Product`, `Year`, CAST(`Revenue` as float) as `Revenue`, CAST(`Quantity` AS int) as `Quantity` from dfs.test.`/salesparquet/*` limit 10;
```


### Scaling

Scaling Drill couldn't be easier, and is especially effective when using Drill with Hadoop. 

To scale run:

```
juju add-unit -n x apache-drill
```

Where x is the number of new units you require.

You can then run 

```
juju status
```

or watch the status pane in the GUI to monitor progress.


## Find out more

[Spicule](http://spicule.co.uk)
[Apache Drill](http://drill.apache.org)
