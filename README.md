# Saiku Enterprise Analytics with Apache Drill

Are you tired of hard to tune, over priced, over complicated analytics platforms? [Saiku Analytics Enterprise Edition](http://meteorite.bi/products/saiku) provides a flexible experience that allows you to dig deeper into your data without writing complex SQL queries. 

Combining Saiku Analytics with the scalable power of Apache Drill, we give our users the ability to query a multitude of data sources including:

* CSV
* Log files
* SQL Datasources
* Hadoop
* S3 data

and more.

Apache Drill and Saiku are a perfect combination for data discovery on an enterprise scale.

Find the data points you need efficently and intuitively with our web based, drag and drop interface. Leverage the power of Juju to connect to different data sources with zero configuration, enabling you to discover data, faster.

## Analyse and explore data.

Apache Drill allows you to query a number of less traditional datasources. As detailed above these do not have to be SQL databases and instead might be, CSV files, JSON files, data stored in Hadoop or a combination of all 3 and more. Apache Drill allows users to query multiple data sources as a single entitiy, letting you combine customer data from your CRM with sales exports data stored on a shared file server in a single view. Allowing users the ability to gain better insight into their data than ever before. 

## User driven dashboarding.

Give users the ability to create their own dashboards from Saiku reports. Using the Saiku Dashboard Designer users can build and deploy their own flexible, parameter driven dashboards, without writing a single line of code. Filter reports in unison with combined filters, show the data your users want with the minimum of fuss.

# Usage

## Give Saiku to everyone.

Saiku is designed to be as easy to deploy as it is to use. Saiku is 100% thin client. It works on any modern browser on PC and Mac. Saiku can easily be integrated into existing security frameworks and is optimised to run on commodity server hardware even with large user communities. Intelligent caching reduces the performance impact on the underlying database and minimises network traffic.
This bundle is a basic Saiku Enterprise Analytics deployment with Apache Drill. It is designed to allow easy deployment of a scalable NOSQL OLAP analysis setup. The deployment of this bundle will deploy the following units:

* 1 Saiku Analytics Enterprise Edition
* 1 Apache Drill
* 1 Apache Zookeeper

## Deployment

There are 2 easy ways to deploy this bundle. 

We deploy a single Zookeeper node. For a redundant system a minimum of 3 Zookeeper nodes should be deployed.

### GUI 

Click the Add to model button at the top of this page. Then the Deploy changes button and follow the on screen instructions.


### Command Line

Deploy this bundle using juju:

juju deploy ~spiculecharms/saiku-drill
juju expose saikuanalytics-enterprise


## Interacting with the bundle

To make use of Saiku you need data. By default the Apache Drill installation is empty, add some data to it!

..........
